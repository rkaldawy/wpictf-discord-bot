import discord
import asyncio
from queue import Queue

class TSQueue():
    def __init__(self):
        self.queue = []
        self.lock = asyncio.Lock()

    async def enqueue(self, item):
        async with self.lock:
            if item not in self.queue:
                self.queue = self.queue + [item]
                print(self.queue)
            return len(self.queue)

    async def dequeue(self):
        async with self.lock:
            print(self.queue)
            if len(self.queue) == 0: return None

            elt = self.queue[0]
            print(elt)
            self.queue = self.queue[1:]
            print("Done retrieving from queue!")
            return elt

    async def remove(self, item):
        async with self.lock:
            try:
                self.queue.remove(item)
                return
            except ValueError:
                return

    async def find_position(self, item):
        async with self.lock:
            try:
                return self.queue.index(item) + 1
            except ValueError:
                return None


TOKEN = ''

tsqueue = TSQueue()
client = discord.Client()

#Server ID
server_id = "433343109875761152"
server = None

#Channel IDs
tech_support_channel = "565079954627231746"
tech_support_queue_channel = "565068989080797194"
general_channel = "433343109875761156"

#ROLE IDs
tech_support_role = "565078725058887710"
administrator_role = "433345229672939521"

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    print("We are picking up the event.")

    if message.author == client.user:
        return
    admin = discord.utils.get(server.roles, id=administrator_role)
    if admin in message.author.roles:
        print("Hai")
        if message.content.startswith("!next_call"):
            role = discord.utils.get(server.roles, id=tech_support_role)
            print(role)

            voice_members = client.get_channel(tech_support_channel).voice_members
            for member in voice_members:
                if admin not in member.roles:
                    await client.remove_roles(member, role)
                    await client.move_member(member, client.get_channel(general_channel))

            n = await tsqueue.dequeue()
            if n is None:
                msg = "{} The tech support queue is empty."
                msg = msg.format(message.author.mention)
                await message.channel.send(msg)
                return
            n = server.get_member(n)

            await client.add_roles(n, role)
            await client.move_member(n, client.get_channel(tech_support_channel))

        elif message.content.startswith("!summon"):
            target_name = message.content.split(" ")[1]
            target_member = server.get_member_named(target_name)

            if target_member is None:
                msg = "{} Member {} does not exist or is not currently online."
                msg = msg.format(message.author.mention, target_name)
                await client.send_message(message.channel, msg)
                return

            role = discord.utils.get(server.roles, id=tech_support_role)

            voice_members = client.get_channel(tech_support_channel).voice_members
            for member in voice_members:
                if admin not in member.roles:
                    await client.remove_roles(member, role)
                    await client.move_member(member, client.get_channel(general_channel))

            await tsqueue.remove(target_member.id)
            await client.add_roles(target_member, role)
            await client.move_member(target_member, client.get_channel(tech_support_channel))
            pass

    if message.content.startswith("!queue_status"):
        pos = await tsqueue.find_position(message.author.id)
        if pos is None:
            msg = "{} You are not currently in the queue."
            msg = msg.format(message.author.mention)
            await client.send_message(message.author, msg)
            return
        msg = "{} You are currently {} call(s) away in the queue."
        msg = msg.format(message.author.mention, pos)
        await client.send_message(message.author, msg)
        return
    pass


@client.event
async def on_voice_state_update(before, after):
    try:
        f = before.voice.voice_channel.name
    except AttributeError:
        f = None
    try:
        t = after.voice.voice_channel.name
    except AttributeError:
        t = None

    if f != "tech-support-line" and t == "tech-support-line":
        if f == "tech-support":
            role = discord.utils.get(server.roles, id=tech_support_role)
            await client.remove_roles(after, role)

        pos = await tsqueue.enqueue(after.id)
        msg = "{} Welcome to the tech support queue. You are {} in line for tech support."
        msg = msg.format(after.mention, pos)
        await client.send_message(after, msg)
        pass
    elif f == "tech-support-line" and t != "tech-support-line":
        if t == "tech-support":
            return
        await tsqueue.remove(after.id)
        pass
    elif f == "tech-support" and t != "tech-support":
        role = discord.utils.get(server.roles, id=tech_support_role)
        await client.remove_roles(after, role)
    else:
        pass

    print(after.voice.voice_channel)


@client.event
async def on_ready():
    global server

    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

    server = client.get_server(server_id)
    print("The server is: ", server)

    channel = client.get_channel(tech_support_queue_channel)
    voice = await client.join_voice_channel(channel)
    player = await voice.create_ytdl_player('https://www.youtube.com/watch?v=6g4dkBF5anU')
    player.start()


#discord.opus.load_opus()
client.run(TOKEN)